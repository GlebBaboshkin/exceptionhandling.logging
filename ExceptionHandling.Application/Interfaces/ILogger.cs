﻿using System;


namespace Calculation.Interfaces
{
	public interface ILogger
	{
		//private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
		void Error(Exception ex);
		void Info(string message);
		void Trace(string message);
	}

    public class Logger : ILogger
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public void Error(Exception ex)
        {
            logger.Error(ex, "Error: exception");
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Trace(string message)
        {
            //logger.Trace(message);
            logger.Trace(message);

        }
    }
}