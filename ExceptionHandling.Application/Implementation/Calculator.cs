﻿using System.Linq;
using Calculation.Interfaces;


namespace Calculation.Implementation
{   
	public class Calculator : BaseCalculator, ICalculator
	{
		private readonly ILogger logger1;
		
		public Calculator(ILogger logger)
		{
			logger1 = logger;
		}

		public int Sum(params int[] numbers)
		{
			System.Text.StringBuilder message = new System.Text.StringBuilder(" ");
			foreach (var i in numbers)
			{ message.Append(i); }
			logger1.Trace(nameof(Calculator.Sum) + " " + message.ToString());


					
			try
			{
				var sum = SafeSum(numbers);
				//System.Text.StringBuilder message1 = new System.Text.StringBuilder(" ");
				//message1.Append(sum);
				logger1.Info( nameof(Calculator.Sum) +" "+ message.ToString()+" " + sum.ToString());
				return sum;
			}
			catch(System.Exception ex)
			{
				logger1.Error(ex); 
				throw; }
           finally { logger1.Trace(nameof(Calculator.Sum)); }
		}

		public int Sub(int a, int b)
		{
            try {
			return SafeSub(a, b); }
			catch { throw new System.OverflowException();}
		}

		public int Multiply(params int[] numbers)
		{
            try
            {
                if (!numbers.Any())
			{
				return 0;
				//throw new System.InvalidOperationException("InvalidOperationExeption", new System.Exception());
			}
			else { return SafeMultiply(numbers); }
            }
            catch(System.Exception ex)
            {
                //System.Exception ex = new System.Exception();

                throw new System.InvalidOperationException("InvalidOperationExeption", ex);
            }

        }

		public int Div(int a, int b)
        {
			try
			{
				try
				{
					return a / b;
				}
				catch (System.Exception ex)
				{ throw new System.InvalidOperationException("", ex); }
			}
			catch (System.InvalidOperationException)
			{ throw new System.InvalidOperationException(); }
		}
	}
}